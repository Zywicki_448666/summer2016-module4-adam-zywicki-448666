# Module 4: Baseball Stat Counter
# Author: Adam Zywicki, 448666
# -*- coding: utf-8 -*-

import re
import sys, os
import operator

# Checking argument parameters
if len(sys.argv) < 2:
	sys.exit("Usage: %s filename" % sys.argv[0])

filename = sys.argv[1]

if not os.path.exists(filename):
	sys.exit("Error: File '%s' not found" % sys.argv[1])

# Function for finding player in list
def search(playerName):
	for player in records:
		if player['name'] == playerName:
			return player

# Function for updating player record
def update(playerName, atBats, hit):
	player = search(playerName)
	if player is None:
		newPlayer = {'name':playerName,'bats':atBats,'hits':hit}
		records.append(newPlayer)
	else:
		player['bats'] += atBats
		player['hits'] += hit


# Reading in file contents
try:
	file = open(filename, "r")
except IOError:
	print 'cannot open', filename
else:
	# Creating arrays for data and sorted data
	players = {}
	sortedBattingAvg = {}
	
	# Reading in lines of file
	lines = [line.strip() for line in open(filename)]
	lineFormat = re.compile('(?P<name>[\w\s]+)\sbatted\s(?P<bats>\d)[\w\s]+with\s(?P<hits>\d)')
	for i in lines:
		result = lineFormat.match(i)
		if result is not None:
			playerName = result.group('name')
			atBats = int(result.group('bats'))
			hits = int(result.group('hits'))
			if atBats is not None and hits is not None:
				record = (hits, atBats)
			if playerName is not None:
				if playerName not in players:
					players[playerName] = record
				else:
					playerStats = players[playerName]
					newStats = (playerStats[0] + hits, playerStats[1] + atBats)
					players[playerName] = newStats

	# Calculating Averages
	for playerStats in players:
		battingAverage = round(float(players[playerStats][0]) / float(players[playerStats][1]),3) 
		players[playerStats] = battingAverage
	
	sortedBattingAvg = sorted(players.iteritems(), key=operator.itemgetter(1), reverse=True)

	# Printing season averages
	print "Players' Averages for the Season:"
	print "_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-"
	for i, (player, battingAverage) in enumerate(sortedBattingAvg):
		print player,":", '{0:.3f}'.format(battingAverage)
	print "_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-"

file.close()


